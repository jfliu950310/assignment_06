//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleApp13
{
    using System;
    using System.Collections.Generic;
    
    public partial class InstType
    {
        public InstType()
        {
            this.Instruments = new HashSet<Instrument>();
        }
    
        public int Id { get; set; }
        public string TypeName { get; set; }
    
        public virtual ICollection<Instrument> Instruments { get; set; }
    }
}
