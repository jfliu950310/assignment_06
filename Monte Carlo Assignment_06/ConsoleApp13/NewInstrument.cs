﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp13
{
    public partial class NewInstrument : Form
    {
        public NewInstrument()
        {
            InitializeComponent();
        }
        
        private void NewInstrument_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'monteCarloDatabaseDataSet1.Instruments' table. You can move, or remove it, as needed.
            this.instrumentsTableAdapter.Fill(this.monteCarloDatabaseDataSet1.Instruments);
            List<string> insttypes = new List<string>();
            var instype =
                from ins in Program.MYC.InstTypes
                select ins.TypeName;
            foreach (string ins in instype)
                insttypes.Add(ins);
            comboBox1.DataSource = insttypes;
            comboBox1.Text = "";

            List<string> under = new List<string>();
            var undertypes =
                from un in Program.MYC.Instruments
                where un.InstTypeId == 1
                select un.Ticker;
            foreach (string un in undertypes)
                under.Add(un);
            comboBox2.DataSource = under;
            comboBox2.Text = "";
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Instrument instrument = new Instrument()
                {
                    CompanyName = textBox1.Text,
                    Ticker = textBox2.Text,
                    Exchange = textBox3.Text,
                    Underlying = (comboBox1.SelectedItem.ToString() == "Stock") ? "N/A" : comboBox2.SelectedItem.ToString(),
                    Strike = (comboBox1.SelectedItem.ToString() == "Stock") ? 0 : Convert.ToDouble(textBox5.Text),
                    Tenor = (comboBox1.SelectedItem.ToString() == "Stock") ? 0 : Convert.ToDouble(textBox6.Text),
                    Rebate = (comboBox1.SelectedItem.ToString() == "Digital Option") ? Convert.ToDouble(textBox7.Text) : 0,
                    BarrierType = (comboBox1.SelectedItem.ToString() == "Barrier Option") ? comboBox3.SelectedItem.ToString() : "N/A",
                    Barrier = (comboBox1.SelectedItem.ToString() == "Barrier Option")? Convert.ToDouble(textBox4.Text):0,
                    IsCall = (comboBox1.SelectedItem.ToString()=="Stock")? "N/A" : (radioButton1.Checked == true) ? "Call" : "Put",
                    InstTypeId = (from InstrTypes in Program.MYC.InstTypes
                                  where InstrTypes.TypeName == comboBox1.SelectedItem.ToString()
                                  select InstrTypes.Id).FirstOrDefault()
                };
                Program.MYC.Instruments.Add(instrument);
                Program.MYC.SaveChanges();
            }
            catch(FormatException)
            {
                MessageBox.Show("Invalid Input");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Utilities.ResetAllControls(this);
            comboBox1.Text = "";

            List<string> under = new List<string>();
            var undertypes =
                from un in Program.MYC.Instruments
                where un.InstTypeId == 1
                select un.Ticker;
            foreach (string un in undertypes)
                under.Add(un);
            comboBox2.DataSource = under;
            comboBox2.Text = "";
            comboBox3.Text = "";
            radioButton1.Checked = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<object> instlist = new List<object>();
            var insts =
                from ins in Program.MYC.Instruments
                select new { ins.Id, ins.CompanyName, ins.Ticker, ins.Exchange, ins.Underlying, ins.Strike, ins.Tenor, ins.IsCall, ins.Rebate, ins.BarrierType, ins.Barrier };
            foreach (object ins in insts)
                instlist.Add(ins);
            dataGridView1.DataSource = instlist;
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }
    }
}
