﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Microsoft.Office.Interop.Owc11;

namespace ConsoleApp13
{
    public partial class PricePlot : Form
    {
        public PricePlot()
        {
            InitializeComponent();
        }

        private void PricePlot_Load(object sender, EventArgs e)
        {
            List<string> stocks = new List<string>();
            var ss = from sts in Program.MYC.Instruments
                     where sts.InstTypeId == 1
                     select sts.Ticker;
            foreach (string sts in ss)
                stocks.Add(sts);
            comboBox1.DataSource = stocks;
            chart1.Series.Clear();
            chart1.ChartAreas[0].AxisX.ScaleView.Zoom(0, 10);
            chart1.ChartAreas[0].AxisY.ScaleView.Zoom(0, 150);
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            Series pr = new Series("price");
            List<double> price = new List<double>();
            int insid = (from ins in Program.MYC.Instruments
                         where ins.Ticker == comboBox1.SelectedItem.ToString()
                         select ins.Id).FirstOrDefault();
            var prices = from pp in Program.MYC.Prices
                         where pp.InstrumentId == insid
                         select pp.ClosingPrice;
            foreach (object pp in prices)
                price.Add(Convert.ToDouble(pp));
            for(int i = 0;i<price.Count;i++)
            {
                pr.Points.AddXY(i, price[i]);
                pr.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            }
            chart1.Series.Add(pr);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
