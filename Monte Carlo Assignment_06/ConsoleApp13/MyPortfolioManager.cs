﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MonteCarloSimulator;

namespace ConsoleApp13
{
    public partial class MyPortfolioManager : Form
    {
        int iden;
        string isbuy;
        Int32 quant;
        double price;
        string timestamp;
        double rebate;
        string barriertype;
        double barrier;
        double tenor;
        double underlying;
        string iscall;
        int insId;
        int instype;
        double pl;
        double markprice;
        double delta;
        double gamma;
        double theta;
        double vega;
        double rho;

        public MyPortfolioManager()
        {
            InitializeComponent();
        }

        private void instrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewInstrument newinst = new NewInstrument();
            newinst.ShowDialog();
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void MyPortfolioManager_Load(object sender, EventArgs e)
        {
            Value.vol = Convert.ToDouble(textBox1.Text);
            Value.step = 10;
            Value.trials = 10000;
        }

        private void tradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewTrade newtrade = new NewTrade();
            newtrade.ShowDialog();
        }

        private void interestRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            intrate newrate = new intrate();
            newrate.ShowDialog();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void historicalPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoricalPrice mp = new HistoricalPrice();
            mp.ShowDialog();
        }

        private void instrumentTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewType nt = new NewType();
            nt.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void button4_Click(object sender, EventArgs e)
        {
            List<object> tradelists = new List<object>();
            var trades = from trade in Program.MYC.Trades
                         select new
                         {
                             trade.Id,
                             trade.IsBuy,
                             trade.Quantity,
                             trade.Price,
                             trade.Timestamp,
                             trade.InstrumentId,
                             trade.Ticker,
                             trade.PL,
                             trade.MarketPrice,
                             trade.Delta,
                             trade.Gamma,
                             trade.Theta,
                             trade.Vega,
                             trade.Rho
                         };
            foreach (object trade in trades)
                tradelists.Add(trade);
            dataGridView1.DataSource = tradelists;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Value.vol = Convert.ToDouble(textBox1.Text);
            dataGridView1.MultiSelect = false;
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select one row");
            }
            else
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Selected == true)
                    {
                        iden = Convert.ToInt16(dataGridView1.Rows[i].Cells[0].Value);
                        isbuy = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        quant = Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value);
                        price = Convert.ToDouble(dataGridView1.Rows[i].Cells[3].Value);
                        timestamp = dataGridView1.Rows[i].Cells[4].ToString();
                        insId = Convert.ToInt16(dataGridView1.Rows[i].Cells[5].Value);
                        instype = (from ins in Program.MYC.Instruments
                                   where ins.Id == insId
                                   select ins.InstTypeId).FirstOrDefault();
                        Value.K = (from st in Program.MYC.Instruments
                                   where st.Id == insId
                                   select st.Strike).FirstOrDefault();
                        tenor = (from st in Program.MYC.Instruments
                                 where st.Id == insId
                                 select st.Tenor).FirstOrDefault();
                        underlying = Convert.ToDouble(textBox2.Text);
                        rebate = (from st in Program.MYC.Instruments
                                  where st.Id == insId
                                  select st.Rebate).FirstOrDefault();
                        barriertype = (from ins in Program.MYC.Instruments
                                       where ins.Id == insId
                                       select ins.BarrierType).FirstOrDefault();
                        barrier = (from st in Program.MYC.Instruments
                                   where st.Id == insId
                                   select st.Barrier).FirstOrDefault();
                        iscall = (from st in Program.MYC.Instruments
                                  where st.Id == insId
                                  select st.IsCall).FirstOrDefault();
                        var tmax = Program.MYC.InterestRates.Max(c => c.Tenor);
                        var tmin = Program.MYC.InterestRates.Min(c => c.Tenor);
                        if (tenor < tmin)
                        {
                            var t1 = tmin;
                            var t2 = Program.MYC.InterestRates.Where(c => c.Tenor > t1).Min(c => c.Tenor);
                            double r1 = (from rates in Program.MYC.InterestRates
                                         where rates.Tenor == t1
                                         select rates.Rate).FirstOrDefault();
                            double r2 = (from rates in Program.MYC.InterestRates
                                         where rates.Tenor == t2
                                         select rates.Rate).FirstOrDefault();
                            Value.r = (Convert.ToDouble(r2) - Convert.ToDouble(r1)) * (tenor - Convert.ToDouble(t1)) / (Convert.ToDouble(t2) - Convert.ToDouble(t1)) + Convert.ToDouble(r1);
                        }
                        else if (tenor > tmax)
                        {
                            var t1 = tmax;
                            var t2 = Program.MYC.InterestRates.Where(c => c.Tenor < t1).Max(c => c.Tenor);
                            double r1 = (from rates in Program.MYC.InterestRates
                                         where rates.Tenor == t1
                                         select rates.Rate).FirstOrDefault();
                            double r2 = (from rates in Program.MYC.InterestRates
                                         where rates.Tenor == t2
                                         select rates.Rate).FirstOrDefault();
                            Value.r = (Convert.ToDouble(r2) - Convert.ToDouble(r1)) * (tenor - Convert.ToDouble(t1)) / (Convert.ToDouble(t2) - Convert.ToDouble(t1)) + Convert.ToDouble(r1);
                        }
                        else
                        {
                            var t1 = Program.MYC.InterestRates.Where(c => c.Tenor <= tenor).Max(c => c.Tenor);
                            var t2 = Program.MYC.InterestRates.Where(c => c.Tenor >= tenor).Min(c => c.Tenor);
                            double r1 = (from rates in Program.MYC.InterestRates
                                         where rates.Tenor == t1
                                         select rates.Rate).FirstOrDefault();
                            double r2 = (from rates in Program.MYC.InterestRates
                                         where rates.Tenor == t2
                                         select rates.Rate).FirstOrDefault();
                            Value.r = (Convert.ToDouble(t1) == Convert.ToDouble(t2)) ? Convert.ToDouble(r1) : (Convert.ToDouble(r2) - Convert.ToDouble(r1)) * (tenor - Convert.ToDouble(t1)) / (Convert.ToDouble(t2) - Convert.ToDouble(t1)) + Convert.ToDouble(r1);
                        }
                    }
                }
                switch (instype.ToString())
                {
                    case "2":
                        European eu = new European();
                        eu.RandomGenerated();
                        markprice = (iscall == "Call") ? eu.CallPrice(underlying, Value.r, Value.vol, tenor) : eu.PutPrice(underlying, Value.r, Value.vol, tenor);
                        delta = (iscall == "Call") ? eu.calldelta(underlying, Value.r, Value.vol, tenor) : eu.putdelta(underlying, Value.r, Value.vol, tenor);
                        gamma = (iscall == "Call") ? eu.callgamma(underlying, Value.r, Value.vol, tenor) : eu.putgamma(underlying, Value.r, Value.vol, tenor);
                        theta = (iscall == "Call") ? eu.calltheta(underlying, Value.r, Value.vol, tenor) : eu.puttheta(underlying, Value.r, Value.vol, tenor);
                        vega = (iscall == "Call") ? eu.callvega(underlying, Value.r, Value.vol, tenor) : eu.putvega(underlying, Value.r, Value.vol, tenor);
                        rho = (iscall == "Call") ? eu.callrho(underlying, Value.r, Value.vol, tenor) : eu.putrho(underlying, Value.r, Value.vol, tenor);
                        pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        break;
                    case "3":
                        Asian asi = new Asian();
                        asi.RandomGenerated();
                        markprice = (iscall == "Call") ? asi.CallPrice(underlying, Value.r, Value.vol, tenor) : asi.PutPrice(underlying, Value.r, Value.vol, tenor);
                        delta = (iscall == "Call") ? asi.calldelta(underlying, Value.r, Value.vol, tenor) : asi.putdelta(underlying, Value.r, Value.vol, tenor);
                        gamma = (iscall == "Call") ? asi.callgamma(underlying, Value.r, Value.vol, tenor) : asi.putgamma(underlying, Value.r, Value.vol, tenor);
                        theta = (iscall == "Call") ? asi.calltheta(underlying, Value.r, Value.vol, tenor) : asi.puttheta(underlying, Value.r, Value.vol, tenor);
                        vega = (iscall == "Call") ? asi.callvega(underlying, Value.r, Value.vol, tenor) : asi.putvega(underlying, Value.r, Value.vol, tenor);
                        rho = (iscall == "Call") ? asi.callrho(underlying, Value.r, Value.vol, tenor) : asi.putrho(underlying, Value.r, Value.vol, tenor);
                        pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        break;
                    case "4":
                        Digital di = new Digital();
                        Value.rebate = rebate;
                        di.RandomGenerated();
                        markprice = (iscall == "Call") ? di.CallPrice(underlying, Value.r, Value.vol, tenor) : di.PutPrice(underlying, Value.r, Value.vol, tenor);
                        delta = (iscall == "Call") ? di.calldelta(underlying, Value.r, Value.vol, tenor) : di.putdelta(underlying, Value.r, Value.vol, tenor);
                        gamma = (iscall == "Call") ? di.callgamma(underlying, Value.r, Value.vol, tenor) : di.putgamma(underlying, Value.r, Value.vol, tenor);
                        theta = (iscall == "Call") ? di.calltheta(underlying, Value.r, Value.vol, tenor) : di.puttheta(underlying, Value.r, Value.vol, tenor);
                        vega = (iscall == "Call") ? di.callvega(underlying, Value.r, Value.vol, tenor) : di.putvega(underlying, Value.r, Value.vol, tenor);
                        rho = (iscall == "Call") ? di.callrho(underlying, Value.r, Value.vol, tenor) : di.putrho(underlying, Value.r, Value.vol, tenor);
                        pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        break;
                    case "5":
                        if (barriertype == "Down and Out")
                        {
                            Value.barrier = barrier;
                            BarrierDAO bdao = new BarrierDAO();
                            bdao.RandomGenerated();
                            markprice = (iscall == "Call") ? bdao.CallPrice(underlying, Value.r, Value.vol, tenor) : bdao.PutPrice(underlying, Value.r, Value.vol, tenor);
                            delta = (iscall == "Call") ? bdao.calldelta(underlying, Value.r, Value.vol, tenor) : bdao.putdelta(underlying, Value.r, Value.vol, tenor);
                            gamma = (iscall == "Call") ? bdao.callgamma(underlying, Value.r, Value.vol, tenor) : bdao.putgamma(underlying, Value.r, Value.vol, tenor);
                            theta = (iscall == "Call") ? bdao.calltheta(underlying, Value.r, Value.vol, tenor) : bdao.puttheta(underlying, Value.r, Value.vol, tenor);
                            vega = (iscall == "Call") ? bdao.callvega(underlying, Value.r, Value.vol, tenor) : bdao.putvega(underlying, Value.r, Value.vol, tenor);
                            rho = (iscall == "Call") ? bdao.callrho(underlying, Value.r, Value.vol, tenor) : bdao.putrho(underlying, Value.r, Value.vol, tenor);
                            pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        }
                        else if (barriertype == "Down and In")
                        {
                            Value.barrier = barrier;
                            BarrierDAI bdai = new BarrierDAI();
                            bdai.RandomGenerated();
                            markprice = (iscall == "Call") ? bdai.CallPrice(underlying, Value.r, Value.vol, tenor) : bdai.PutPrice(underlying, Value.r, Value.vol, tenor);
                            delta = (iscall == "Call") ? bdai.calldelta(underlying, Value.r, Value.vol, tenor) : bdai.putdelta(underlying, Value.r, Value.vol, tenor);
                            gamma = (iscall == "Call") ? bdai.callgamma(underlying, Value.r, Value.vol, tenor) : bdai.putgamma(underlying, Value.r, Value.vol, tenor);
                            theta = (iscall == "Call") ? bdai.calltheta(underlying, Value.r, Value.vol, tenor) : bdai.puttheta(underlying, Value.r, Value.vol, tenor);
                            vega = (iscall == "Call") ? bdai.callvega(underlying, Value.r, Value.vol, tenor) : bdai.putvega(underlying, Value.r, Value.vol, tenor);
                            rho = (iscall == "Call") ? bdai.callrho(underlying, Value.r, Value.vol, tenor) : bdai.putrho(underlying, Value.r, Value.vol, tenor);
                            pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        }
                        else if (barriertype == "Up and Out")
                        {
                            Value.barrier = barrier;
                            BarrierUAO buao = new BarrierUAO();
                            buao.RandomGenerated();
                            markprice = (iscall == "Call") ? buao.CallPrice(underlying, Value.r, Value.vol, tenor) : buao.PutPrice(underlying, Value.r, Value.vol, tenor);
                            delta = (iscall == "Call") ? buao.calldelta(underlying, Value.r, Value.vol, tenor) : buao.putdelta(underlying, Value.r, Value.vol, tenor);
                            gamma = (iscall == "Call") ? buao.callgamma(underlying, Value.r, Value.vol, tenor) : buao.putgamma(underlying, Value.r, Value.vol, tenor);
                            theta = (iscall == "Call") ? buao.calltheta(underlying, Value.r, Value.vol, tenor) : buao.puttheta(underlying, Value.r, Value.vol, tenor);
                            vega = (iscall == "Call") ? buao.callvega(underlying, Value.r, Value.vol, tenor) : buao.putvega(underlying, Value.r, Value.vol, tenor);
                            rho = (iscall == "Call") ? buao.callrho(underlying, Value.r, Value.vol, tenor) : buao.putrho(underlying, Value.r, Value.vol, tenor);
                            pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        }
                        else if (barriertype == "Up and In")
                        {
                            Value.barrier = barrier;
                            BarrierUAI buai = new BarrierUAI();
                            buai.RandomGenerated();
                            markprice = (iscall == "Call") ? buai.CallPrice(underlying, Value.r, Value.vol, tenor) : buai.PutPrice(underlying, Value.r, Value.vol, tenor);
                            delta = (iscall == "Call") ? buai.calldelta(underlying, Value.r, Value.vol, tenor) : buai.putdelta(underlying, Value.r, Value.vol, tenor);
                            gamma = (iscall == "Call") ? buai.callgamma(underlying, Value.r, Value.vol, tenor) : buai.putgamma(underlying, Value.r, Value.vol, tenor);
                            theta = (iscall == "Call") ? buai.calltheta(underlying, Value.r, Value.vol, tenor) : buai.puttheta(underlying, Value.r, Value.vol, tenor);
                            vega = (iscall == "Call") ? buai.callvega(underlying, Value.r, Value.vol, tenor) : buai.putvega(underlying, Value.r, Value.vol, tenor);
                            rho = (iscall == "Call") ? buai.callrho(underlying, Value.r, Value.vol, tenor) : buai.putrho(underlying, Value.r, Value.vol, tenor);
                            pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        }
                        break;
                    case "6":
                        Lookback lo = new Lookback();
                        lo.RandomGenerated();
                        markprice = (iscall == "Call") ? lo.CallPrice(underlying, Value.r, Value.vol, tenor) : lo.PutPrice(underlying, Value.r, Value.vol, tenor);
                        delta = (iscall == "Call") ? lo.calldelta(underlying, Value.r, Value.vol, tenor) : lo.putdelta(underlying, Value.r, Value.vol, tenor);
                        gamma = (iscall == "Call") ? lo.callgamma(underlying, Value.r, Value.vol, tenor) : lo.putgamma(underlying, Value.r, Value.vol, tenor);
                        theta = (iscall == "Call") ? lo.calltheta(underlying, Value.r, Value.vol, tenor) : lo.puttheta(underlying, Value.r, Value.vol, tenor);
                        vega = (iscall == "Call") ? lo.callvega(underlying, Value.r, Value.vol, tenor) : lo.putvega(underlying, Value.r, Value.vol, tenor);
                        rho = (iscall == "Call") ? lo.callrho(underlying, Value.r, Value.vol, tenor) : lo.putrho(underlying, Value.r, Value.vol, tenor);
                        pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        break;
                    case "7":
                        Range ra = new Range();
                        ra.RandomGenerated();
                        markprice = (iscall == "Call") ? ra.CallPrice(underlying, Value.r, Value.vol, tenor) : ra.PutPrice(underlying, Value.r, Value.vol, tenor);
                        delta = (iscall == "Call") ? ra.calldelta(underlying, Value.r, Value.vol, tenor) : ra.putdelta(underlying, Value.r, Value.vol, tenor);
                        gamma = (iscall == "Call") ? ra.callgamma(underlying, Value.r, Value.vol, tenor) : ra.putgamma(underlying, Value.r, Value.vol, tenor);
                        theta = (iscall == "Call") ? ra.calltheta(underlying, Value.r, Value.vol, tenor) : ra.puttheta(underlying, Value.r, Value.vol, tenor);
                        vega = (iscall == "Call") ? ra.callvega(underlying, Value.r, Value.vol, tenor) : ra.putvega(underlying, Value.r, Value.vol, tenor);
                        rho = (iscall == "Call") ? ra.callrho(underlying, Value.r, Value.vol, tenor) : ra.putrho(underlying, Value.r, Value.vol, tenor);
                        pl = (isbuy == "Buy") ? (markprice - price) * quant : (price - markprice) * quant;
                        break;
                    default:
                        MessageBox.Show("Please check the data");
                        break;
                }
               var trade1 = Program.MYC.Trades.FirstOrDefault(c => c.Id == iden);
                trade1.PL = pl;
                trade1.MarketPrice = markprice;
                trade1.Delta = delta;
                trade1.Gamma = gamma;
                trade1.Theta = theta;
                trade1.Vega = vega;
                trade1.Rho = rho;
                Program.MYC.SaveChanges();
                List<object> tradelists = new List<object>();
                var trades = from trade in Program.MYC.Trades
                             select new
                             {
                                 trade.Id,
                                 trade.IsBuy,
                                 trade.Quantity,
                                 trade.Price,
                                 trade.Timestamp,
                                 trade.InstrumentId,
                                 trade.Ticker,
                                 trade.PL,
                                 trade.MarketPrice,
                                 trade.Delta,
                                 trade.Gamma,
                                 trade.Theta,
                                 trade.Vega,
                                 trade.Rho
                             };
                foreach (object trade in trades)
                    tradelists.Add(trade);
                dataGridView1.DataSource = tradelists;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<double> pl = new List<double>();
            List<double> delta = new List<double>();
            List<double> gamma = new List<double>();
            List<double> theta = new List<double>();
            List<double> vega = new List<double>();
            List<double> rho = new List<double>();
            for (int i =0;i<dataGridView1.Rows.Count;i++)
            {
                if(dataGridView1.Rows[i].Selected == true)
                {
                    pl.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[7].Value));
                    delta.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[9].Value));
                    gamma.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[10].Value));
                    theta.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[11].Value));
                    vega.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[12].Value));
                    rho.Add(Convert.ToDouble(dataGridView1.Rows[i].Cells[13].Value));
                }
            }
            double totalpl = pl.Sum();
            double totaldelta = delta.Sum();
            double totalgamma = gamma.Sum();
            double totaltheta = theta.Sum();
            double totalvega = vega.Sum();
            double totalrho = rho.Sum();
            Program.MYC.Totals.Add(new Total()
            {
                TotalPL = totalpl,
                TotalDelta = totaldelta,
                TotalGamma = totalgamma,
                TotalTheta = totaltheta,
                TotalVega = totalvega,
                TotalRho = totalrho
            });
            Program.MYC.SaveChanges();
            var tots = from m in Program.MYC.Totals
                       select new
            {
                totalPL = m.TotalPL,
                totaldelta = m.TotalDelta,
                totalgamma = m.TotalGamma,
                totaltheta = m.TotalTheta,
                totalvega = m.TotalVega,
                totalrho = m.TotalRho
            };
            List<object> tdata = new List<object>();
            foreach (object m in tots)
                tdata.Add(m);
            dataGridView2.DataSource = tdata;
            var tot = Program.MYC.Totals.FirstOrDefault();
            Program.MYC.Totals.Remove(tot);
            Program.MYC.SaveChanges();
        }

        private void historicalPricePlotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PricePlot pp = new PricePlot();
            pp.ShowDialog();
                
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1. You should add the instrument types and instruments first. When adding the instruments types, please follows this order:" + "\n" +
                             "    Stock, European Option, Asian Option, Digital Option, Barrier Option, Lookback Option, Range Option" + "\n" +
                             "2.  You can only delete one row in 'Trade' form every time you want to delete record." + "\n" +
                             "3.  You should add the price before you add trades. Meanwhile, when you add the trades, you must have the price on the same day, or there will be an error." + "\n" +
                             "4.  The call and put should have different tickers. Please using the different tickers to identify them." + "\n" +
                             "5.  If you want to view the records of anything you added, please click 'Refresh' in each form." + "\n" +
                             "6.  You can only delete trades in this GUI.");
        }
    }
    public class Utilities
    {
        public static void ResetAllControls(Control form)
        {
            foreach (Control control in form.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.Text = null;
                }

                if (control is ComboBox)
                {
                    ComboBox comboBox = (ComboBox)control;
                    if (comboBox.Items.Count > 0)
                        comboBox.SelectedIndex = 0;
                }

                if (control is CheckBox)
                {
                    CheckBox checkBox = (CheckBox)control;
                    checkBox.Checked = false;
                }
            }
        }
    }
}
