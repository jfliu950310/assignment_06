﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp13
{
    public partial class intrate : Form
    {
        public intrate()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Program.MYC.InterestRates.Add(new InterestRate()
                {
                    Tenor =Convert.ToDouble(textBox1.Text),
                    Rate = Convert.ToDouble(textBox2.Text)
                });
                Program.MYC.SaveChanges();
            }
            catch(FormatException)
            {
                MessageBox.Show("Invalid Input");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Utilities.ResetAllControls(this);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<object> intrate = new List<object>();
            var rates =
                from r in Program.MYC.InterestRates
                select new { r.Id, r.Tenor, r.Rate };
            foreach (object r in rates)
                intrate.Add(r);
            dataGridView1.DataSource = intrate;
        }
    }
}
