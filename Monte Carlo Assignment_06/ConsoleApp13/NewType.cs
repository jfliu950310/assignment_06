﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp13
{
    public partial class NewType : Form
    {
        public NewType()
        {
            InitializeComponent();
        }

        private void NewType_Load(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.MYC.InstTypes.Add(new InstType()
            {
                TypeName = textBox1.Text
            });
            Program.MYC.SaveChanges();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<object> typelist = new List<object>();
            var types =
                from t in Program.MYC.InstTypes
                select new { t.Id, t.TypeName };
            foreach (object t in types)
                typelist.Add(t);
            dataGridView1.DataSource = typelist;
        }
    }
}
