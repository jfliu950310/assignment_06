﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MonteCarloSimulator;

namespace ConsoleApp13
{
    public partial class NewTrade : Form
    {
        public NewTrade()
        {
            InitializeComponent();
        }

        public static string isBuy;
        public static Int32 quantity;
        public static double price;
        public static string timestamp;
        public static int instrumentId;
        public static int instrumentType;
        public static double marketPrice;
        public static string iscall;
        public static double PL;
        public static double delta;
        public static double gamma;
        public static double theta;
        public static double vega;
        public static double rho;

        private void label4_Click(object sender, EventArgs e)
        {

        }
    
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                isBuy = comboBox2.SelectedItem.ToString();
                quantity = Convert.ToInt32(textBox2.Text);
                price = Convert.ToDouble(textBox5.Text);
                timestamp = dateTimePicker1.Text;
                instrumentId = (from insts in Program.MYC.Instruments
                                where insts.Ticker == comboBox1.SelectedItem.ToString()
                                select insts.Id).FirstOrDefault();
                instrumentType = (from insts in Program.MYC.Instruments
                                  where insts.Ticker == comboBox1.SelectedItem.ToString()
                                  select insts.InstTypeId).FirstOrDefault();
                if (instrumentType == 1)
                {
                    var traderecord = new Trade()
                    {
                        IsBuy = isBuy,
                        Quantity = quantity,
                        Price =price,
                        Timestamp =timestamp,
                        InstrumentId = instrumentId,
                        Ticker = comboBox1.SelectedItem.ToString(),
                        PL= (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity),
                        MarketPrice = (from p in Program.MYC.Prices
                                      where p.Date == timestamp       
                                      select p.ClosingPrice).FirstOrDefault(),
                        Delta =1,
                        Gamma =0,
                        Theta =0,
                        Vega=0,
                        Rho=0
                    };
                    Program.MYC.Trades.Add(traderecord);
                    Program.MYC.SaveChanges();
                }
                else
                {
                    Value.S = (from hisprice in Program.MYC.Prices
                               where hisprice.Date == timestamp
                               select hisprice.ClosingPrice).FirstOrDefault();
                    Value.K = (from insts in Program.MYC.Instruments
                               where insts.Ticker == comboBox1.SelectedItem.ToString()
                               select insts.Strike).FirstOrDefault();
                    Value.t = (from insts in Program.MYC.Instruments
                               where insts.Ticker == comboBox1.SelectedItem.ToString()
                               select insts.Tenor).FirstOrDefault();
                    var tmax = Program.MYC.InterestRates.Max(c => c.Tenor);
                    var tmin = Program.MYC.InterestRates.Min(c => c.Tenor);
                    if (Value.t < tmin)
                    {
                        var t1 = tmin;
                        var t2 = Program.MYC.InterestRates.Where(c => c.Tenor > t1).Min(c => c.Tenor);
                        double r1 = (from rates in Program.MYC.InterestRates
                                     where rates.Tenor == t1
                                     select rates.Rate).FirstOrDefault();
                        double r2 = (from rates in Program.MYC.InterestRates
                                     where rates.Tenor == t2
                                     select rates.Rate).FirstOrDefault();
                        Value.r = (Convert.ToDouble(r2) - Convert.ToDouble(r1)) * (Value.t - Convert.ToDouble(t1)) / (Convert.ToDouble(t2) - Convert.ToDouble(t1)) + Convert.ToDouble(r1);
                    }
                    else if (Value.t > tmax)
                    {
                        var t1 = tmax;
                        var t2 = Program.MYC.InterestRates.Where(c => c.Tenor < t1).Max(c => c.Tenor);
                        double r1 = (from rates in Program.MYC.InterestRates
                                     where rates.Tenor == t1
                                     select rates.Rate).FirstOrDefault();
                        double r2 = (from rates in Program.MYC.InterestRates
                                     where rates.Tenor == t2
                                     select rates.Rate).FirstOrDefault();
                        Value.r = (Convert.ToDouble(r2) - Convert.ToDouble(r1)) * (Value.t - Convert.ToDouble(t1)) / (Convert.ToDouble(t2) - Convert.ToDouble(t1)) + Convert.ToDouble(r1);
                    }
                    else
                    {
                        var t1 = Program.MYC.InterestRates.Where(c => c.Tenor <= Value.t).Max(c => c.Tenor);
                        var t2 = Program.MYC.InterestRates.Where(c => c.Tenor >= Value.t).Min(c => c.Tenor);
                        double r1 = (from rates in Program.MYC.InterestRates
                                     where rates.Tenor == t1
                                     select rates.Rate).FirstOrDefault();
                        double r2 = (from rates in Program.MYC.InterestRates
                                                 where rates.Tenor == t2
                                                 select rates.Rate).FirstOrDefault();
                        Value.r = (Convert.ToDouble(t1) == Convert.ToDouble(t2)) ? Convert.ToDouble(r1) : (Convert.ToDouble(r2) - Convert.ToDouble(r1)) * (Value.t - Convert.ToDouble(t1)) / (Convert.ToDouble(t2) - Convert.ToDouble(t1)) + Convert.ToDouble(r1);
                    }
                    iscall = (from insts in Program.MYC.Instruments
                              where insts.Ticker == comboBox1.SelectedItem.ToString()
                              select insts.IsCall).FirstOrDefault();
                    switch(instrumentType.ToString())
                    {
                        case "2":
                            European eu = new European();
                            eu.RandomGenerated();
                            marketPrice = (iscall == "Call") ? eu.CallPrice(Value.S, Value.r, Value.vol,Value.t) : eu.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                            delta = (iscall == "Call") ? eu.calldelta(Value.S, Value.r, Value.vol, Value.t) : eu.putdelta(Value.S, Value.r, Value.vol, Value.t);
                            gamma = (iscall == "Call") ? eu.callgamma(Value.S, Value.r, Value.vol, Value.t) : eu.putgamma(Value.S, Value.r, Value.vol, Value.t);
                            theta = (iscall == "Call") ? eu.calltheta(Value.S, Value.r, Value.vol, Value.t) : eu.puttheta(Value.S, Value.r, Value.vol, Value.t);
                            vega = (iscall == "Call") ? eu.callvega(Value.S, Value.r, Value.vol, Value.t) : eu.putvega(Value.S, Value.r, Value.vol, Value.t);
                            rho = (iscall == "Call") ? eu.callrho(Value.S, Value.r, Value.vol, Value.t) : eu.putrho(Value.S, Value.r, Value.vol, Value.t);
                            PL = (isBuy == "Buy") ? (marketPrice - price)*Convert.ToDouble(quantity) : (price - marketPrice)*Convert.ToDouble(quantity);
                            break;
                        case "3":
                            Asian asian = new Asian();
                            asian.RandomGenerated();
                            marketPrice = (iscall == "Call") ? asian.CallPrice(Value.S, Value.r, Value.vol, Value.t) : asian.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                            delta = (iscall == "Call") ? asian.calldelta(Value.S, Value.r, Value.vol, Value.t) : asian.putdelta(Value.S, Value.r, Value.vol, Value.t);
                            gamma = (iscall == "Call") ? asian.callgamma(Value.S, Value.r, Value.vol, Value.t) : asian.putgamma(Value.S, Value.r, Value.vol, Value.t);
                            theta = (iscall == "Call") ? asian.calltheta(Value.S, Value.r, Value.vol, Value.t) : asian.puttheta(Value.S, Value.r, Value.vol, Value.t);
                            vega = (iscall == "Call") ? asian.callvega(Value.S, Value.r, Value.vol, Value.t) : asian.putvega(Value.S, Value.r, Value.vol, Value.t);
                            rho = (iscall == "Call") ? asian.callrho(Value.S, Value.r, Value.vol, Value.t) : asian.putrho(Value.S, Value.r, Value.vol, Value.t);
                            PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            break;
                        case "4":
                            Digital di = new Digital();
                            Value.rebate = (from ints in Program.MYC.Instruments
                                            where ints.Ticker == comboBox1.SelectedItem.ToString()
                                            select ints.Rebate).FirstOrDefault();
                            di.RandomGenerated();
                            marketPrice = (iscall == "Call") ? di.CallPrice(Value.S, Value.r, Value.vol, Value.t) : di.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                            delta = (iscall == "Call") ? di.calldelta(Value.S, Value.r, Value.vol, Value.t) : di.putdelta(Value.S, Value.r, Value.vol, Value.t);
                            gamma = (iscall == "Call") ? di.callgamma(Value.S, Value.r, Value.vol, Value.t) : di.putgamma(Value.S, Value.r, Value.vol, Value.t);
                            theta = (iscall == "Call") ? di.calltheta(Value.S, Value.r, Value.vol, Value.t) : di.puttheta(Value.S, Value.r, Value.vol, Value.t);
                            vega = (iscall == "Call") ? di.callvega(Value.S, Value.r, Value.vol, Value.t) : di.putvega(Value.S, Value.r, Value.vol, Value.t);
                            rho = (iscall == "Call") ? di.callrho(Value.S, Value.r, Value.vol, Value.t) : di.putrho(Value.S, Value.r, Value.vol, Value.t);
                            PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            break;
                        case "5":
                            string barriertype = (from ints in Program.MYC.Instruments
                                                  where ints.Ticker == comboBox1.SelectedItem.ToString()
                                                  select ints.BarrierType).FirstOrDefault();
                            Value.barrier = (from ints in Program.MYC.Instruments
                                             where ints.Ticker == comboBox1.SelectedItem.ToString()
                                             select ints.Barrier).FirstOrDefault();
                            if(barriertype == "Down and Out")
                            {
                                BarrierDAO badao = new BarrierDAO();
                                badao.RandomGenerated();
                                marketPrice = (iscall == "Call") ? badao.CallPrice(Value.S, Value.r, Value.vol, Value.t) : badao.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                                delta = (iscall == "Call") ? badao.calldelta(Value.S, Value.r, Value.vol, Value.t) : badao.putdelta(Value.S, Value.r, Value.vol, Value.t);
                                gamma = (iscall == "Call") ? badao.callgamma(Value.S, Value.r, Value.vol, Value.t) : badao.putgamma(Value.S, Value.r, Value.vol, Value.t);
                                theta = (iscall == "Call") ? badao.calltheta(Value.S, Value.r, Value.vol, Value.t) : badao.puttheta(Value.S, Value.r, Value.vol, Value.t);
                                vega = (iscall == "Call") ? badao.callvega(Value.S, Value.r, Value.vol, Value.t) : badao.putvega(Value.S, Value.r, Value.vol, Value.t);
                                rho = (iscall == "Call") ? badao.callrho(Value.S, Value.r, Value.vol, Value.t) : badao.putrho(Value.S, Value.r, Value.vol, Value.t);
                                PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            }
                            else if(barriertype == "Down and In")
                            {
                                BarrierDAI badai = new BarrierDAI();
                                badai.RandomGenerated();
                                marketPrice = (iscall == "Call") ? badai.CallPrice(Value.S, Value.r, Value.vol, Value.t) : badai.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                                delta = (iscall == "Call") ? badai.calldelta(Value.S, Value.r, Value.vol, Value.t) : badai.putdelta(Value.S, Value.r, Value.vol, Value.t);
                                gamma = (iscall == "Call") ? badai.callgamma(Value.S, Value.r, Value.vol, Value.t) : badai.putgamma(Value.S, Value.r, Value.vol, Value.t);
                                theta = (iscall == "Call") ? badai.calltheta(Value.S, Value.r, Value.vol, Value.t) : badai.puttheta(Value.S, Value.r, Value.vol, Value.t);
                                vega = (iscall == "Call") ? badai.callvega(Value.S, Value.r, Value.vol, Value.t) : badai.putvega(Value.S, Value.r, Value.vol, Value.t);
                                rho = (iscall == "Call") ? badai.callrho(Value.S, Value.r, Value.vol, Value.t) : badai.putrho(Value.S, Value.r, Value.vol, Value.t);
                                PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            }
                            else if(barriertype == "Up and Out")
                            {
                                BarrierUAO bauao = new BarrierUAO();
                                bauao.RandomGenerated();
                                marketPrice = (iscall == "Call") ? bauao.CallPrice(Value.S, Value.r, Value.vol, Value.t) : bauao.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                                delta = (iscall == "Call") ? bauao.calldelta(Value.S, Value.r, Value.vol, Value.t) : bauao.putdelta(Value.S, Value.r, Value.vol, Value.t);
                                gamma = (iscall == "Call") ? bauao.callgamma(Value.S, Value.r, Value.vol, Value.t) : bauao.putgamma(Value.S, Value.r, Value.vol, Value.t);
                                theta = (iscall == "Call") ? bauao.calltheta(Value.S, Value.r, Value.vol, Value.t) : bauao.puttheta(Value.S, Value.r, Value.vol, Value.t);
                                vega = (iscall == "Call") ? bauao.callvega(Value.S, Value.r, Value.vol, Value.t) : bauao.putvega(Value.S, Value.r, Value.vol, Value.t);
                                rho = (iscall == "Call") ? bauao.callrho(Value.S, Value.r, Value.vol, Value.t) : bauao.putrho(Value.S, Value.r, Value.vol, Value.t);
                                PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            }
                            else if(barriertype == "Up and In")
                            {
                                BarrierUAI bauai = new BarrierUAI();
                                bauai.RandomGenerated();
                                marketPrice = (iscall == "Call") ? bauai.CallPrice(Value.S, Value.r, Value.vol, Value.t) : bauai.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                                delta = (iscall == "Call") ? bauai.calldelta(Value.S, Value.r, Value.vol, Value.t) : bauai.putdelta(Value.S, Value.r, Value.vol, Value.t);
                                gamma = (iscall == "Call") ? bauai.callgamma(Value.S, Value.r, Value.vol, Value.t) : bauai.putgamma(Value.S, Value.r, Value.vol, Value.t);
                                theta = (iscall == "Call") ? bauai.calltheta(Value.S, Value.r, Value.vol, Value.t) : bauai.puttheta(Value.S, Value.r, Value.vol, Value.t);
                                vega = (iscall == "Call") ? bauai.callvega(Value.S, Value.r, Value.vol, Value.t) : bauai.putvega(Value.S, Value.r, Value.vol, Value.t);
                                rho = (iscall == "Call") ? bauai.callrho(Value.S, Value.r, Value.vol, Value.t) : bauai.putrho(Value.S, Value.r, Value.vol, Value.t);
                                PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            }
                            break;
                        case "6":
                            Lookback lo = new Lookback();
                            lo.RandomGenerated();
                            marketPrice = (iscall == "Call") ? lo.CallPrice(Value.S, Value.r, Value.vol, Value.t) : lo.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                            delta = (iscall == "Call") ? lo.calldelta(Value.S, Value.r, Value.vol, Value.t) : lo.putdelta(Value.S, Value.r, Value.vol, Value.t);
                            gamma = (iscall == "Call") ? lo.callgamma(Value.S, Value.r, Value.vol, Value.t) : lo.putgamma(Value.S, Value.r, Value.vol, Value.t);
                            theta = (iscall == "Call") ? lo.calltheta(Value.S, Value.r, Value.vol, Value.t) : lo.puttheta(Value.S, Value.r, Value.vol, Value.t);
                            vega = (iscall == "Call") ? lo.callvega(Value.S, Value.r, Value.vol, Value.t) : lo.putvega(Value.S, Value.r, Value.vol, Value.t);
                            rho = (iscall == "Call") ? lo.callrho(Value.S, Value.r, Value.vol, Value.t) : lo.putrho(Value.S, Value.r, Value.vol, Value.t);
                            PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            break;
                        case "7":
                            Range ra = new Range();
                            ra.RandomGenerated();
                            marketPrice = (iscall == "Call") ? ra.CallPrice(Value.S, Value.r, Value.vol, Value.t) : ra.PutPrice(Value.S, Value.r, Value.vol, Value.t);
                            delta = (iscall == "Call") ? ra.calldelta(Value.S, Value.r, Value.vol, Value.t) : ra.putdelta(Value.S, Value.r, Value.vol, Value.t);
                            gamma = (iscall == "Call") ? ra.callgamma(Value.S, Value.r, Value.vol, Value.t) : ra.putgamma(Value.S, Value.r, Value.vol, Value.t);
                            theta = (iscall == "Call") ? ra.calltheta(Value.S, Value.r, Value.vol, Value.t) : ra.puttheta(Value.S, Value.r, Value.vol, Value.t);
                            vega = (iscall == "Call") ? ra.callvega(Value.S, Value.r, Value.vol, Value.t) : ra.putvega(Value.S, Value.r, Value.vol, Value.t);
                            rho = (iscall == "Call") ? ra.callrho(Value.S, Value.r, Value.vol, Value.t) : ra.putrho(Value.S, Value.r, Value.vol, Value.t);
                            PL = (isBuy == "Buy") ? (marketPrice - price) * Convert.ToDouble(quantity) : (price - marketPrice) * Convert.ToDouble(quantity);
                            break;
                        default:
                            MessageBox.Show("Please check the data");
                            break;
                    }
                    var traderecord = new Trade()
                    {
                        IsBuy = isBuy,
                        Quantity = quantity,
                        Price = price,
                        Timestamp = timestamp,
                        InstrumentId = instrumentId,
                        Ticker = comboBox1.SelectedItem.ToString(),
                        PL = PL,
                        MarketPrice = marketPrice,
                        Delta = delta,
                        Gamma = gamma,
                        Theta = theta,
                        Vega = vega,
                        Rho = rho
                    };
                    Program.MYC.Trades.Add(traderecord);
                    Program.MYC.SaveChanges();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid value");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Utilities.ResetAllControls(this);
            List<string> insts = new List<string>();
            var insttype =
                from ins in Program.MYC.Instruments
                select ins.Ticker;
            foreach (string ins in insttype)
                insts.Add(ins);
            comboBox1.DataSource = insts;
            comboBox1.Text = "";
            comboBox2.Text = "";
        }

        private void NewTrade_Load(object sender, EventArgs e)
        {
            List<string> insts = new List<string>();
            var insttype =
                from ins in Program.MYC.Instruments
                select ins.Ticker;
            foreach (string ins in insttype)
                insts.Add(ins);
            comboBox1.DataSource = insts;
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            List<object> ob = new List<object>();
            var trade =
                from trades in Program.MYC.Trades
                select new
                {
                    trades.Id,
                    trades.IsBuy,
                    trades.Quantity,
                    trades.Price,
                    trades.Timestamp,
                    trades.InstrumentId,
                    trades.Ticker,
                    trades.PL,
                    trades.MarketPrice,
                    trades.Delta,
                    trades.Gamma,
                    trades.Theta,
                    trades.Vega,
                    trades.Rho
                };
            foreach (object trades in trade)
                ob.Add(trades);
            dataGridView1.DataSource = ob;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            int ide = 0;
            for(int i=0;i<dataGridView1.Rows.Count;i++)
            {
                if (dataGridView1.Rows[i].Selected == true)
                    ide = Convert.ToInt16(dataGridView1.Rows[i].Cells[0].Value);
            }
            var trade = Program.MYC.Trades.FirstOrDefault(c => c.Id == ide);
            Program.MYC.Trades.Remove(trade);
            Program.MYC.SaveChanges();
        }
    }
}
