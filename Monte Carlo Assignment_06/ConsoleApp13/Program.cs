﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp13
{
    class Program
    {
        public static MyModelContainer1 MYC = new MyModelContainer1();
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MyPortfolioManager mymanager = new MyPortfolioManager();
            Application.Run(mymanager);
        }
    }
}
