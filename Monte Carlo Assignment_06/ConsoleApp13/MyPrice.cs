﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp13
{
    public partial class HistoricalPrice : Form
    {
        public HistoricalPrice()
        {
            InitializeComponent();
        }

        private void HistoricalPrice_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'monteCarloDatabaseDataSet2.Prices' table. You can move, or remove it, as needed.
            this.pricesTableAdapter.Fill(this.monteCarloDatabaseDataSet2.Prices);
            List<string> comitem = new List<string>();
            var inst =
                from ins in Program.MYC.Instruments
                select ins.Ticker;
            foreach (string ins in inst)
                comitem.Add(ins);
            comboBox1.DataSource = comitem;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Program.MYC.Prices.Add(new Price
                {
                    Date = dateTimePicker1.Text,
                    ClosingPrice = Convert.ToDouble(textBox1.Text),
                    InstrumentId = (from instr in Program.MYC.Instruments
                                   where instr.Ticker == comboBox1.SelectedItem.ToString()
                                   select instr.Id).FirstOrDefault()
                });
                Program.MYC.SaveChanges();
            }
            catch(FormatException)
            {
                MessageBox.Show("Invalid Input");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<object> pricelist = new List<object>();
            int insid = (from inst in Program.MYC.Instruments
                         where inst.Ticker == comboBox1.SelectedItem.ToString()
                         select inst.Id).FirstOrDefault();
            var pricelists =
                from price in Program.MYC.Prices
                where price.InstrumentId == insid
                select new { price.Id, price.Date, price.ClosingPrice };
            foreach (object price in pricelists)
                pricelist.Add(price);
            dataGridView1.DataSource = pricelist;
        }
    }
}
